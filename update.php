<?php
include_once 'db-config.php';
if($_GET['id']){
	$customer_id= $_GET['id'];
	$sql = "SELECT * FROM user WHERE id = '$customer_id'";
	$result=mysqli_query($conn,$sql);
	$data = mysqli_fetch_assoc($result);
	//input data 
	$Name= $data['name'];
	$Email= $data['email'];
	$Phone= $data['phone'];
	$Address= $data['address'];
	
	if($_POST){
		extract($_POST);
		$query = "UPDATE user SET name='$name', email='$email', phone = '$phone', address='$address' WHERE id=$id";
		$stmt= mysqli_query($conn,$query);
		if($stmt){
			//header("Location:edit_customer.php?msg= Data Update Successfully");
			//session
			session_start();
			$_SESSION['message']="Data Update Successfully";
			header("Location:edit_customer.php");
		}
	}
}


 ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SHOP</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-default">
	  <div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="index.php">Shop Management</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav navbar-right">
			<li><a href="index.php">Customer</a></li>
			<li><a href="#">Supplier</a></li>
			<li><a href="#">Product Category</a></li>
			<li><a href="#">Product details</a></li>
			<!--
			<li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
			  <ul class="dropdown-menu">
				<li><a href="#">Action</a></li>
				<li><a href="#">Another action</a></li>
				<li><a href="#">Something else here</a></li>
				<li role="separator" class="divider"></li>
				<li><a href="#">Separated link</a></li>
			  </ul>
			</li>-->
		  </ul>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	
	<section>
		<div class="container">			
			<div class="row">
				<div class="col-md-3">
					<div class="left-sidebar">
						<ul class="nav nav-stacked">
						  <li role="presentation"><a href="index.php">Creat Customer</a></li>
						  <li role="presentation"><a href="view_customer.php">View Customer</a></li>
						  <li role="presentation" class="active"><a href="edit_customer.php">Edit Customer</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-9">
					<div class="text-center"><h1>Update Customer</h1></div><br>
					
					<form class="form-horizontal" action="" method="post">
					<input type="hidden" name="id" value="<?php echo $customer_id ?>">
					  <div class="form-group">
						<label class="col-sm-2 control-label">Name</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" name="name" value="<?php echo $Name; ?>" placeholder="Kamrul">
						</div>
					  </div>
					  <div class="form-group">
						<label class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
						  <input type="email" class="form-control" name="email" value="<?php echo $Email; ?>" placeholder="kamrul.bitm61@gmail.com">
						</div>
					  </div>
					  <div class="form-group">
						<label class="col-sm-2 control-label">Phone</label>
						<div class="col-sm-10">
						  <input type="number" class="form-control" name="phone" value="<?php echo $Phone; ?>" placeholder="01989442316">
						</div>
					  </div>
					  <div class="form-group">
						<label class="col-sm-2 control-label">Address</label>
						<div class="col-sm-10">						  
						  <textarea   class="form-control" name="address" value="<?php echo $Address; ?>" placeholder="Mirpur 10, Dhaka, Bangladesh"></textarea>
						</div>
					  </div>
				 
					  <div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
						  <button type="submit" class="btn btn-primary btn-lg btn-block">Update</button>
						</div>
					  </div>
					</form>
				</div>
			</div>		
		</div>
	</section>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>