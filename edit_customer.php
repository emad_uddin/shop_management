<?php 
session_start();
include_once 'db-config.php';
//database query
$sql = "SELECT * FROM user";
$result= mysqli_query($conn, $sql);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SHOP</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-default">
	  <div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="index.php">Shop Management</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav navbar-right">
			<li><a href="index.php">Customer</a></li>
			<li><a href="#">Supplier</a></li>
			<li><a href="#">Product Category</a></li>
			<li><a href="#">Product details</a></li>
			<!--
			<li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
			  <ul class="dropdown-menu">
				<li><a href="#">Action</a></li>
				<li><a href="#">Another action</a></li>
				<li><a href="#">Something else here</a></li>
				<li role="separator" class="divider"></li>
				<li><a href="#">Separated link</a></li>
			  </ul>
			</li>-->
		  </ul>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	
	<section>
		<div class="container">			
			<div class="row">
				<div class="col-md-3">
					<div class="left-sidebar">
						<ul class="nav nav-stacked">
						  <li role="presentation"><a href="index.php">Creat Customer</a></li>
						  <li role="presentation"><a href="view_customer.php">View Customer</a></li>
						  <li role="presentation" class="active"><a href="edit_customer.php">Edit Customer</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-9">
					<div class="text-center"><h1>Edit Customer</h1></div><br>
					<?php if (isset($_SESSION["message"])){ ?>
						<div class="alert alert-success"><?php echo $_SESSION["message"]; unset($_SESSION["message"]); ?></div>
					<?php } ?>
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<th>SI. NO.</th>
							<th>NAME</th>
							<th>E-MAIL</th>
							<th>PHONE</th>
							<th>ADDRESS</th>
							<th>IMAGE</th>
							<th>Action</th>
							<?php while($customer = mysqli_fetch_assoc($result)){?>
							<tr>
								<td><?php echo $customer['id'];?></td>
								<td><?php echo $customer['name'];?></td>
								<td><?php echo $customer['email'];?></td>
								<td><?php echo $customer['phone'];?></td>
								<td><?php echo $customer['address'];?></td>
								<td><img width="100" src="uploads/<?php echo $customer['file'] ?>"></td>
								<td>
									<a class="btn btn-success btn-sm" href="update.php?id=<?php echo $customer['id']; ?>" role="button"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Edit</a>
									<a class="btn btn-danger btn-sm" href= "del_customer.php?id=<?php echo $customer['id']; ?>"  onclick="return confirm('Do You Want To Delete it?')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>
								</td>
							</tr>
							<?php }?>
						</table>
					</div>
				</div>
			</div>		
		</div>
	</section>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<script>
		function customerDelete() {
			alert("Confirm To Delete!");
		}
	</script>
  </body>
</html>