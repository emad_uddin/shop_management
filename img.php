<?php 
$host ="localhost";
$user = "root";
$pass="";
$database="full_shop";

$conn = mysqli_connect($host,$user,$pass,$database);
if(!$conn){
	die("connection failed:".mysqli_connect_error());
}

if(isset($_POST['img_submit']))
{    
     
	$file = rand(1000,100000)."-".$_FILES['file']['name'];
    $file_loc = $_FILES['file']['tmp_name'];
	$file_size = $_FILES['file']['size'];
	$file_type = $_FILES['file']['type'];
	$folder="uploads/";
	
	move_uploaded_file($file_loc,$folder.$final_file);
		$sql="INSERT INTO user(file,type,size) VALUES('$file','$file_type','$file_size')";
		$image= mysqli_query($conn, $sql);
		
		if($image){
			echo "query success";
	}else{
		echo "query not success";
	}
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SHOP</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-default">
	  <div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand" href="index.php">Shop Management</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		  <ul class="nav navbar-nav navbar-right">
			<li><a href="index.php">Customer</a></li>
			<li><a href="#">Supplier</a></li>
			<li><a href="#">Product Category</a></li>
			<li><a href="#">Product Details</a></li>
			<!--
			<li class="dropdown">
			  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
			  <ul class="dropdown-menu">
				<li><a href="#">Action</a></li>
				<li><a href="#">Another action</a></li>
				<li><a href="#">Something else here</a></li>
				<li role="separator" class="divider"></li>
				<li><a href="#">Separated link</a></li>
			  </ul>
			</li>-->
		  </ul>
		</div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	
	<section>
		<div class="container">			
			<div class="row">
				<div class="col-md-3">
					<div class="left-sidebar">
						<ul class="nav nav-stacked">
						  <li role="presentation" class="active"><a href="index.php">Creat Customer</a></li>
						  <li role="presentation"><a href="view_customer.php">View Customer</a></li>
						  <li role="presentation"><a href="edit_customer.php">Edit Customer</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-9">
					<div class="text-center"><h1>Customer Registration Form</h1></div><br>
					<?php if (isset($msg)){ ?>
						<div class="alert alert-success"><?php echo $msg; ?></div>
					<?php } ?>

					<form class="form-horizontal" action="" method="post" name="myForm" onsubmit="return(validate());" >
					  
					 
					 
					  <div class="form-group">
						<label class="col-sm-2 control-label">Image</label>
						<div class="col-sm-10">						  
						 <input type="file" name="file" placeholder="image">
						</div>
					  </div>
				 
					  <div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
						  <button type="submit" name="img_submit" class="btn btn-success btn-lg btn-block">Submit</button>
						</div>
					  </div>
					</form>
				</div>
			</div>		
		</div>
	</section>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-3.2.1.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	
	<script type="text/javascript">
	   <!--
		  // Form validation code will come here.
		  function validate()
		  {
		  
			 if( document.myForm.name.value == "" )
			 {
				alert( "Please provide your name!" );
				document.myForm.name.focus() ;
				return false;
			 }
			 
			 if( document.myForm.email.value == "" )
			 {
				alert( "Please provide your Email!" );
				document.myForm.email.focus() ;
				return false;
			 }
			 if( document.myForm.phone.value == "" )
			 {
				alert( "Please provide your Phone Number!" );
				document.myForm.phone.focus() ;
				return false;
			 }
			 if( document.myForm.address.value == "" )
			 {
				alert( "Please provide your Address!" );
				document.myForm.address.focus() ;
				return false;
			 }
			 		
			 return( true );
		  }
	   //-->
	</script>
  </body>
</html>